"use strict";

const { Contract } = require("fabric-contract-api");

// https://hyperledger.github.io/fabric-chaincode-node/main/api/fabric-shim.ChaincodeStub.html

async function getAllResults(iterator) {
  const allResults = [];
  while (true) {
    const res = await iterator.next();
    if (res.value) {
      // if not a getHistoryForKey iterator then key is contained in res.value.key
      allResults.push(res.value.value.toString("utf8"));
    }

    // check to see if we have reached then end
    if (res.done) {
      // explicitly close the iterator
      await iterator.close();
      return allResults;
    }
  }
}

class Password extends Contract {
  constructor() {
    super();
  }

  async createPassword(ctx, username, service, password, owner) {
    const newPassword = {
      username,
      service,
      password,
      type: "password", // password || note || file
      owner,
    };

    try {
      const key = ctx.stub.createCompositeKey(owner, [
        owner,
        username,
        service,
      ]);
      await ctx.stub.putState(key, Buffer.from(JSON.stringify(newPassword)));
      return {
        data: "Added new password",
      };
    } catch (e) {
      return { error: e.message };
    }
  }

  async changePassword(ctx, username, service, newPassword, owner) {
    try {
      const key = ctx.stub.createCompositeKey(owner, [
        owner,
        username,
        service,
      ]);
      const passwordBytes = await ctx.stub.getState(key);

      if (!passwordBytes || passwordBytes === 0) {
        return { error: "Username or service does not exists" };
      }
      const passwordItem = JSON.parse(passwordBytes.toString());
      passwordItem.password = newPassword;

      await ctx.stub.putState(key, Buffer.from(JSON.stringify(passwordItem)));
      return {
        data: "Password updated",
      };
    } catch (e) {
      return { error: "Username or service does not exists" };
    }
  }

  async queryPassword(ctx, username, service, owner) {
    try {
      const key = ctx.stub.createCompositeKey(owner, [
        owner,
        username,
        service,
      ]);
      const passwordBytes = await ctx.stub.getHistoryForKey(key);

      return {
        data: (await getAllResults(passwordBytes)).map(JSON.parse),
      };
    } catch (e) {
      return {
        error: e.message,
      };
    }
  }

  async allPasswords(ctx, owner) {
    try {
      const passwords = await ctx.stub.getQueryResult(
        JSON.stringify({
          selector: {
            owner: owner,
            type: "password",
          },
        })
      );

      return {
        data: (await getAllResults(passwords)).map(JSON.parse),
      };
    } catch (e) {
      return {
        error: e.message,
      };
    }
  }

  async addNote(ctx, owner, body, id, lastModified, title) {
    const newNote = {
      body,
      id,
      lastModified,
      title,
      type: "note", // password || note || file
      owner,
    };

    try {
      const key = ctx.stub.createCompositeKey(owner, [owner, id]);
      await ctx.stub.putState(key, Buffer.from(JSON.stringify(newNote)));
      return {
        data: "Added new note",
      };
    } catch (e) {
      return { error: e.message };
    }
  }

  async updateNote(ctx, owner, body, id, lastModified, title) {
    try {
      const key = ctx.stub.createCompositeKey(owner, [owner, id]);
      const notesBytes = await ctx.stub.getState(key);

      if (!notesBytes || notesBytes === 0) {
        return { error: "Note id not found" };
      }

      const note = JSON.parse(notesBytes.toString());
      note.body = body;
      note.lastModified = lastModified;
      note.title = title;

      await ctx.stub.putState(key, Buffer.from(JSON.stringify(note)));
      return {
        data: "Note updated",
      };
    } catch (e) {
      return { error: "Note id not found" };
    }
  }

  async queryNote(ctx, owner, id) {
    try {
      const key = ctx.stub.createCompositeKey(owner, [owner, id]);

      const noteBytes = await ctx.stub.getHistoryForKey(key);

      return {
        data: (await getAllResults(noteBytes)).map(JSON.parse),
      };
    } catch (e) {
      return {
        error: e.message,
      };
    }
  }

  async allNotes(ctx, owner) {
    try {
      const notes = await ctx.stub.getQueryResult(
        JSON.stringify({
          selector: {
            owner: owner,
            type: "note",
          },
        })
      );

      return {
        data: (await getAllResults(notes)).map(JSON.parse),
      };
    } catch (e) {
      return {
        error: e.message,
      };
    }
  }

  async uploadFile(ctx, owner, filename, content){
    const newFile={
      filename,content,type:"file",owner
    }

    try{
      const key = ctx.stub.createCompositeKey(owner,[owner,filename])
      await ctx.stub.putState(key,Buffer.from(JSON.stringify(newFile)))
      return {
        data: "File uploaded"
      }

    }catch(e){
      return {error:e.message}
    }
  }

  async allFiles(ctx, owner) {
    try {
      const files = await ctx.stub.getQueryResult(
        JSON.stringify({
          selector: {
            owner: owner,
            type: "file",
          },
        })
      );

      return {
        data: (await getAllResults(files)).map(JSON.parse),
      };
    } catch (e) {
      return {
        error: e.message,
      };
    }
  }
}

module.exports = Password;
