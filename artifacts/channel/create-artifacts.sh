#!/bin/bash

function removeOldMaterial {
    # chmod -R 0755 ./crypto-config
    sudo rm -rf ./crypto-config
    rm -rf genesis.block
    rm sharedchannel.tx
    rm AbcMSPanchors.tx
    rm XyzMSPanchors.tx
}

if [[ $1 == "new" ]]; then
    echo "Delete previous files [y/N] ?"
    read confirm
    if [[ $confirm == "y" ]]; then
        echo -e "\e[1;44m Removing previous files \e[0m"
        removeOldMaterial
    fi
elif [[ $1 == "rm" ]];then
    echo "Delete files [y/N] ?"
    read confirm
    if [[ $confirm == "y" ]]; then
        echo -e "\e[1;44m Removing previous files \e[0m"
        removeOldMaterial
        exit 0
    fi
fi

echo -e "\e[1;44m Generating Crypto artifacts for organizations using ${PWD}/crypto-config.yaml \e[0m"
cryptogen generate --config=$(pwd)/crypto-config.yaml --output=$(pwd)/crypto-config/

SYS_CHANNEL="sys-channel"

# default channel
CHANNEL_NAME="sharedchannel"

echo -e "\e[1;44m Generating System Genesis Block \e[0m"
configtxgen -profile OrdererGenesis -configPath . -channelID $SYS_CHANNEL  -outputBlock ./genesis.block

echo -e "\e[1;44m Generating channel configuration block \e[0m"
configtxgen -profile BasicChannel -configPath . -outputCreateChannelTx ./sharedchannel.tx -channelID $CHANNEL_NAME

echo -e "\e[1;44m Generating anchor peer update for AbcMSP \e[0m"
configtxgen -profile BasicChannel -configPath . -outputAnchorPeersUpdate ./AbcMSPanchors.tx -channelID $CHANNEL_NAME -asOrg AbcMSP

echo -e "\e[1;44m Generating anchor peer update for XyzMSP \e[0m"
configtxgen -profile BasicChannel -configPath . -outputAnchorPeersUpdate ./XyzMSPanchors.tx -channelID $CHANNEL_NAME -asOrg XyzMSP

echo -e "\e[1;42m Created Crypto material \e[0m"