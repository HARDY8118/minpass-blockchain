export CORE_PEER_TLS_ENABLED=true
export ORDERER_CA=${PWD}/artifacts/channel/crypto-config/ordererOrganizations/minpass.com/orderers/orderer.minpass.com/msp/tlscacerts/tlsca.minpass.com-cert.pem
export PEER0_ABC_CA=${PWD}/artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/peers/peer0.abc.minpass.com/tls/ca.crt
export PEER0_XYZ_CA=${PWD}/artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/peers/peer0.xyz.minpass.com/tls/ca.crt
export FABRIC_CFG_PATH=${PWD}/artifacts/channel/config/

export CHANNEL_NAME=sharedchannel

setGlobalsForOrderer(){
    export CORE_PEER_LOCALMSPID="OrdererMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/artifacts/channel/crypto-config/ordererOrganizations/minpass.com/orderers/orderer.minpass.com/msp/tlscacerts/tlsca.minpass.com-cert.pem
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/ordererOrganizations/minpass.com/users/Admin@minpass.com/msp
    
}

setGlobalsForPeer0Abc(){
    export CORE_PEER_LOCALMSPID="AbcMSP"  
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ABC_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/users/Admin@abc.minpass.com/msp
    export CORE_PEER_ADDRESS=localhost:7051
}

setGlobalsForPeer1Abc(){
    export CORE_PEER_LOCALMSPID="AbcMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ABC_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/users/Admin@abc.minpass.com/msp
    export CORE_PEER_ADDRESS=localhost:8051
    
}

setGlobalsForPeer0Xyz(){
    export CORE_PEER_LOCALMSPID="XyzMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_XYZ_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/users/Admin@xyz.minpass.com/msp
    export CORE_PEER_ADDRESS=localhost:9051
    
}

setGlobalsForPeer1Xyz(){
    export CORE_PEER_LOCALMSPID="XyzMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_XYZ_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/users/Admin@xyz.minpass.com/msp
    export CORE_PEER_ADDRESS=localhost:10051
    
}

createChannel(){
    rm -rf ./channel-artifacts/*
    mkdir channel-artifacts
    setGlobalsForPeer0Abc

    peer channel create -o localhost:7050 -c $CHANNEL_NAME --ordererTLSHostnameOverride orderer.minpass.com -f ./artifacts/channel/${CHANNEL_NAME}.tx --outputBlock ./channel-artifacts/${CHANNEL_NAME}.block --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
}


joinChannel(){
    setGlobalsForPeer0Abc
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block
    
    setGlobalsForPeer1Abc
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block
    
    setGlobalsForPeer0Xyz
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block
    
    setGlobalsForPeer1Xyz
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block
    
}

updateAnchorPeers(){
    setGlobalsForPeer0Abc
    peer channel update -o localhost:7050 --ordererTLSHostnameOverride orderer.minpass.com -c $CHANNEL_NAME -f ./artifacts/channel/${CORE_PEER_LOCALMSPID}anchors.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
    
    setGlobalsForPeer0Xyz
    peer channel update -o localhost:7050 --ordererTLSHostnameOverride orderer.minpass.com -c $CHANNEL_NAME -f ./artifacts/channel/${CORE_PEER_LOCALMSPID}anchors.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
    
}

createChannel
joinChannel
updateAnchorPeers