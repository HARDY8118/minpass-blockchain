#!/bin/bash

export CORE_PEER_TLS_ENABLED=true
export ORDERER_CA=${PWD}/artifacts/channel/crypto-config/ordererOrganizations/minpass.com/orderers/orderer.minpass.com/msp/tlscacerts/tlsca.minpass.com-cert.pem
export PEER0_ABC_CA=${PWD}/artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/peers/peer0.abc.minpass.com/tls/ca.crt
export PEER0_XYZ_CA=${PWD}/artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/peers/peer0.xyz.minpass.com/tls/ca.crt
export FABRIC_CFG_PATH=${PWD}/artifacts/channel/config/

export CHANNEL_NAME=sharedchannel

setGlobalsForOrderer(){
    export CORE_PEER_LOCALMSPID="OrdererMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/artifacts/channel/crypto-config/ordererOrganizations/minpass.com/orderers/orderer.minpass.com/msp/tlscacerts/tlsca.minpass.com-cert.pem
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/ordererOrganizations/minpass.com/users/Admin@minpass.com/msp
    
}

setGlobalsForPeer0Abc(){
    export CORE_PEER_LOCALMSPID="AbcMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ABC_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/users/Admin@abc.minpass.com/msp
    export CORE_PEER_ADDRESS=localhost:7051
}

setGlobalsForPeer1Abc(){
    export CORE_PEER_LOCALMSPID="AbcMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ABC_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/users/Admin@abc.minpass.com/msp
    export CORE_PEER_ADDRESS=localhost:8051
    
}

setGlobalsForPeer0Xyz(){
    export CORE_PEER_LOCALMSPID="XyzMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_XYZ_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/users/Admin@xyz.minpass.com/msp
    export CORE_PEER_ADDRESS=localhost:9051
    
}

setGlobalsForPeer1Xyz(){
    export CORE_PEER_LOCALMSPID="XyzMSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_XYZ_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/users/Admin@xyz.minpass.com/msp
    export CORE_PEER_ADDRESS=localhost:10051
    
}

presetup(){
    echo -e "\e[1;44m Installing chaincode dependencies \e[0m"
    pushd ./artifacts/chaincode
    # nvm use 10.15.2
    npm install
    popd
    echo -e "\e[1;42m Installation complete \e[0m"
}
# presetup

if [[ ! -f ./CC_VERSION ]]; then
    echo "1" > ./CC_VERSION
    VERSION="1"
else
    VERSION=$(cat ./CC_VERSION)
    VERSION=$((VERSION + 1))
    echo $VERSION > ./CC_VERSION
fi

CHANNEL_NAME="sharedchannel"
CC_RUNTIME_LANGUAGE="node"
# VERSION="1"
CC_SRC_PATH="./artifacts/chaincode/"
CC_NAME="Password"

packageChaincode(){
    rm -rf ${CC_NAME}.tar.gz
    setGlobalsForPeer0Abc
    peer lifecycle chaincode package ${CC_NAME}.tar.gz --path ${CC_SRC_PATH} --lang ${CC_RUNTIME_LANGUAGE} --label ${CC_NAME}_${VERSION}
    echo -e "\e[1;44m ===================== Chaincode is packaged on peer0.abc ===================== \e[0m"
}

installChaincode(){
    setGlobalsForPeer0Abc
    peer lifecycle chaincode install ${CC_NAME}.tar.gz
    echo -e "\e[1;44m ===================== Chaincode installed on peer0.abc ===================== \e[0m"
    
    setGlobalsForPeer1Abc
    peer lifecycle chaincode install ${CC_NAME}.tar.gz
    echo -e "\e[1;44m ===================== Chaincode installed on peer1.abc ===================== \e[0m"
    
    setGlobalsForPeer0Xyz
    peer lifecycle chaincode install ${CC_NAME}.tar.gz
    echo -e "\e[1;44m ===================== Chaincode installed on peer0.xyz ===================== \e[0m"
    
    setGlobalsForPeer1Xyz
    peer lifecycle chaincode install ${CC_NAME}.tar.gz
    echo -e "\e[1;44m ===================== Chaincode installed on peer1.xyz ===================== \e[0m"
}

queryInstalled(){
    setGlobalsForPeer0Abc
    peer lifecycle chaincode queryinstalled >&log.txt
    cat log.txt
    PACKAGE_ID=$(sed -n "/${CC_NAME}_${VERSION}/{s/^Package ID: //; s/, Label:.*$//; p;}" log.txt)
    echo PackageID is ${PACKAGE_ID}
    echo -e "\e[1;42m ===================== Query installed successful on peer0.abc on channel ===================== \e[0m"
}

approveForMyAbc(){
    setGlobalsForPeer0Abc

    peer lifecycle chaincode approveformyorg -o localhost:7050  \
    --ordererTLSHostnameOverride orderer.minpass.com \
    --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA \
    --channelID $CHANNEL_NAME --name ${CC_NAME} \
    --version ${VERSION} \
    --package-id ${PACKAGE_ID} --sequence ${VERSION}
    # --init-required \

    echo -e "\e[1;42m ===================== Chaincode approved from organization abc ===================== \e[0m"
}

# --signature-policy "OR ('AbcMSP.member')"
# --peerAddresses localhost:7051 --tlsRootCertFiles $PEER0_ABC_CA --peerAddresses localhost:9051 --tlsRootCertFiles $PEER0_XYZ_CA
# --peerAddresses peer0.abc.minpass.com:7051 --tlsRootCertFiles $PEER0_ABC_CA --peerAddresses peer0.xyz.minpass.com:9051 --tlsRootCertFiles $PEER0_XYZ_CA
#--channel-config-policy Channel/Application/Admins
# --signature-policy "OR ('AbcMSP.peer','XyzMSP.peer')"


checkCommitReadyness(){
    setGlobalsForPeer0Abc

    peer lifecycle chaincode checkcommitreadiness \
    --channelID $CHANNEL_NAME \
    --name ${CC_NAME} \
    --version ${VERSION} \
    --sequence ${VERSION} \
    --output json
    # --init-required

    echo -e "\e[1;44m ===================== checking commit readyness from org abc ===================== \e[0m"
}

approveForMyXyz(){
    setGlobalsForPeer0Xyz

    peer lifecycle chaincode approveformyorg -o localhost:7050 \
    --ordererTLSHostnameOverride orderer.minpass.com \
    --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA \
    --channelID $CHANNEL_NAME --name ${CC_NAME} \
    --version ${VERSION} \
    --package-id ${PACKAGE_ID} --sequence ${VERSION}
    # --init-required \

    echo -e "\e[1;42m ===================== Chaincode approved from organization xyz ===================== \e[0m"
}

checkCommitReadyness(){
    
    setGlobalsForPeer0Abc

    peer lifecycle chaincode checkcommitreadiness \
    --channelID $CHANNEL_NAME \
    --peerAddresses localhost:7051 \
    --tlsRootCertFiles $PEER0_ABC_CA \
    --name ${CC_NAME} \
    --version ${VERSION} \
    --sequence ${VERSION} \
    --output json
    # --init-required

    echo -e "\e[1;44m ===================== checking commit readyness from org abc ===================== \e[0m"
}

commitChaincodeDefination(){
    setGlobalsForPeer0Abc

    peer lifecycle chaincode commit -o localhost:7050 \
    --ordererTLSHostnameOverride orderer.minpass.com \
    --tls $CORE_PEER_TLS_ENABLED  --cafile $ORDERER_CA \
    --channelID $CHANNEL_NAME --name ${CC_NAME} \
    --peerAddresses localhost:7051 --tlsRootCertFiles $PEER0_ABC_CA \
    --peerAddresses localhost:9051 --tlsRootCertFiles $PEER0_XYZ_CA \
    --version ${VERSION} --sequence ${VERSION} \
    # --init-required
   
    echo -e "\e[1;44m ===================== Chaincode Defination commited on peer0-org abc and peer0-xyz ===================== \e[0m"
}

queryCommitted(){
    setGlobalsForPeer0Abc

    peer lifecycle chaincode querycommitted --channelID $CHANNEL_NAME --name ${CC_NAME}

    
}

chaincodeInvokeInit(){
    setGlobalsForPeer0Abc

    # peer chaincode invoke -o localhost:7050 \
    # --ordererTLSHostnameOverride orderer.minpass.com \
    # --tls $CORE_PEER_TLS_ENABLED \
    # --cafile $ORDERER_CA -C $CHANNEL_NAME -n ${CC_NAME} \
    # --peerAddresses localhost:7051 \
    # --tlsRootCertFiles $PEER0_ABC_CA \
    # --peerAddresses localhost:9051 \
    # --tlsRootCertFiles $PEER0_XYZ_CA \
    # --isInit -c '{"function":"initLedger","Args":[]}'
    
    peer chaincode invoke -o localhost:7050 \
    --ordererTLSHostnameOverride orderer.minpass.com \
    --tls \
    --cafile $ORDERER_CA -C $CHANNEL_NAME -n $CC_NAME \
    --peerAddresses localhost:7051 \
    --tlsRootCertFiles $PEER0_ABC_CA \
    --peerAddresses localhost:9051 \
    --tlsRootCertFiles $PEER0_XYZ_CA -c '{"function":"initLedger","Args":[]}'

}


chaincodeInvoke(){
    # setGlobalsForPeer0Abc

    # Initialize Ledger
    # peer chaincode invoke -o localhost:7050 \
    # --ordererTLSHostnameOverride orderer.minpass.com \
    # --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n ${CC_NAME} \
    # --peerAddresses localhost:7051 --tlsRootCertFiles $PEER0_ABC_CA \
    # --peerAddresses localhost:9051 --tlsRootCertFiles $PEER0_XYZ_CA  \
    # -c '{"function":"initLedger","Args":[]}'
    
    setGlobalsForPeer0Abc

    ## Create Car
    # peer chaincode invoke -o localhost:7050 \
    #     --ordererTLSHostnameOverride orderer.minpass.com \
    #     --tls $CORE_PEER_TLS_ENABLED \
    #     --cafile $ORDERER_CA \
    #     -C $CHANNEL_NAME -n ${CC_NAME}  \
    #     --peerAddresses localhost:7051 \
    #     --tlsRootCertFiles $PEER0_ABC_CA \
    #     --peerAddresses localhost:9051 --tlsRootCertFiles $PEER0_XYZ_CA $PEER_CONN_PARMS  \
    #     -c '{"function": "CreateCar","Args":["Car-ABCDEEE", "Audi", "R8", "Red", "Pavan"]}'
    
    ## Change car owner
    peer chaincode invoke -o localhost:7050 \
        --ordererTLSHostnameOverride orderer.minpass.com \
        --tls $CORE_PEER_TLS_ENABLED \
        --cafile $ORDERER_CA \
        -C $CHANNEL_NAME -n ${CC_NAME}  \
        --peerAddresses localhost:7051 \
        --tlsRootCertFiles $PEER0_ABC_CA \
        --peerAddresses localhost:9051 --tlsRootCertFiles $PEER0_XYZ_CA $PEER_CONN_PARMS  \
        -c '{"function": "changePassword","Args":["bob", "example.com", "passwd"]}'
}

chaincodeQuery(){
    setGlobalsForPeer0Abc

    # Query all Cars
    # peer chaincode query -C $CHANNEL_NAME -n ${CC_NAME} -c '{"Args":["queryAllCars"]}'

    # Query by Car id
    peer chaincode query -C $CHANNEL_NAME -n ${CC_NAME} -c '{"Args":["queryPassword", "bob", "example.com"]}'
}

presetup
packageChaincode
installChaincode
queryInstalled

approveForMyAbc
checkCommitReadyness
approveForMyXyz
checkCommitReadyness

commitChaincodeDefination
queryCommitted

# echo
# echo
# echo -e "\e[1;44m ===================== Initializing ===================== \e[0m"
# chaincodeInvokeInit
# sleep 5
# chaincodeQuery
# sleep 5
# echo
# echo
# echo -e "\e[1;44m ===================== Invoking ===================== \e[0m"
# chaincodeInvoke
# sleep 5
# echo
# echo
# echo -e "\e[1;44m ===================== Query ===================== \e[0m"
# chaincodeQuery
# sleep 5