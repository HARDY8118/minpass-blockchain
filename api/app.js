const express = require("express");
const bodyParser = require("body-parser");

const helper = require("./helper");

const PORT = 3000;
const app = express();

const winston = require("winston");
const clc = require("cli-color");

const authorize = require("./auth");

const logger = winston.createLogger({
  level: "info",
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.simple()
  ),
  transports: [
    new winston.transports.File({
      filename: require("path").resolve(__dirname, "logs", "api.log"),
    }),
  ],
});

if (process.env.NODE_ENV !== "production") {
  logger.add(
    new winston.transports.Console({
      level: "http",
      format: winston.format.timestamp(),
    })
  );
}

app.use(bodyParser.json());

// app.get("/", (req, res) => {
//   console.log(req.body);
//   res.end("OK");
// });

app.post("/register", async (req, res) => {
  logger.http("[%s] %s", new Date(), clc.bgBlue.white("/register"));
  const { username, organization } = req.body;

  logger.info("[%s] Registering %s in %s", new Date(), username, organization);
  const response = await helper.getUser(username, organization);
  logger.info("[%s] %s", new Date(), response.data);

  res.json(response);
});

app.all("/register", (req, res) => {
  logger.http("[%s] %s", new Date(), clc.bgYellow.white("/register"));
  return res.status(405).send("Method Not Allowed");
});

app.post("/invoke", authorize, async (req, res) => {
  const { channel, chaincode, fn, args, username, organization } = req.body;
  logger.http(
    "[%s] %s %s",
    new Date(),
    clc.bgBlue.white("/invoke"),
    clc.bgWhite.black(chaincode)
  );

  logger.info(
    "[%s] Invoking '%s' of '%s' on '%s' by '%s' of '%s' with '%s'",
    new Date(),
    fn,
    chaincode,
    channel,
    username,
    organization,
    args
  );

  if(!organization){
    logger.error("\n[%s] %s", new Date(), "Undefined organization");
    return res.status(400).send('Must provide organization')
  }

  const response = await helper.invokeTransaction(
    channel,
    chaincode,
    fn,
    args,
    username,
    organization
  );

  if (response.error) {
    logger.error("\n[%s] %s", new Date(), response.error);
    return res.status(404).json(response);
  } else if (response.data) {
    logger.info("[%s] Sending data", new Date());
    return res.status(200).json(response);
  } else {
    return res.status(500).send("Server error");
  }
});

app.all("/invoke", (req, res) => {
  logger.http("[%s] %s", new Date(), clc.bgYellow.white("/invoke"));
  return res.status(405).send("Method Not Allowed");
});

app.all("/ping", (req, res) => {
  logger.http("[%s] %s", new Date(), clc.bgGreen.white("/ping"));
  return (
    res
      .status(200)
      // .header("Content-Type", ")
      .send({ ping: "pong" })
  );
});

app.all("*", (req, res) => {
  logger.http("[%s] %s", new Date(), clc.bgRed.white(req.path));
  return res.status(404).send("Not Found");
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
