const { createHash } = require("crypto");

const hash = (m) =>
  createHash("sha512").update(m).digest("hex").toString("hex");

const profiles = {
  test: "46e2face36d57f938e52ea78244629843190d5191d3a3249e37fdc9f1e2710624140c122ab0868b2219be1ad87eca9786fe0154c159fba4b14ba8243c87ac1c3", // testPa$$
  abcd: "d404559f602eab6fd602ac7680dacbfaadd13630335e951f097af3900e9de176b6db28512f2e000b9d04fba5133e8b1c6e8df59db3a8ab9d60be4b97cc9e81db",
};

const authenticate = (username, password) =>
  !!profiles[username] && profiles[username] == hash(password);

const authorize = (req, res, next) => {
  if (!req.headers.authorization) {
    res.set("WWW-Authenticate", 'Basic realm="Invoke chaincode"');
    return res.sendStatus(401);
  }

  const [username, password] = Buffer.from(
    req.headers.authorization.match(/Basic (.*)/)[1],
    "base64"
  )
    .toString("ascii")
    .split(":");

  if (!authenticate(username, password)) {
    return res.sendStatus(401);
  }
  next();
};

module.exports = authorize;
