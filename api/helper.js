const {
  Gateway,
  Wallets,
  DefaultEventHandlerStrategies,
} = require("fabric-network");
const path = require("path");
const FabricCAClient = require("fabric-ca-client");
const fs = require("fs");
const winston = require("winston");

const logger = winston.createLogger({
  level: "debug",
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.simple()
  ),
  transports: [
    new winston.transports.File({
      filename: path.resolve(__dirname, "logs", "debug.log"),
    }),
  ],
});

const apiLogger = winston.createLogger({
  level: "info",
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.simple()
  ),
  transports: [
    new winston.transports.File({
      filename: path.resolve(__dirname, "logs", "api.log"),
    }),
  ],
});

const getCCP = (org) => {
  try {
    const ccpPath = path.resolve(
      __dirname,
      "config",
      `connection-${org.toLowerCase()}.json`
    );
    const ccp = JSON.parse(fs.readFileSync(ccpPath, "utf8"));
    return ccp;
  } catch (e) {
    throw e;
  }
};

const getCAUrl = (ccp, org) => {
  try {
    return ccp.certificateAuthorities[`ca.${org.toLowerCase}.minpass.com`].url;
  } catch (e) {
    throw e;
  }
};

const getUser = async (username, organization) => {
  const ccp = getCCP(organization);
  const caUrl =
    ccp.certificateAuthorities[`ca.${organization.toLowerCase()}.minpass.com`]
      .url;
  const ca = new FabricCAClient(caUrl);

  const walletPath = path.join(
    process.cwd(),
    `wallet-${organization.toLowerCase()}`
  );
  const wallet = await Wallets.newFileSystemWallet(walletPath);

  const userIdentity = await wallet.get(username);

  if (userIdentity) {
    return {
      data: username + " already exists in " + organization,
    };
  }

  let adminIdentity = await wallet.get("admin");
  if (!adminIdentity) {
    try {
      apiLogger.info("[%s] Registering Admin", new Date());
      await enrollAdmin(organization, ccp);
      adminIdentity = await wallet.get("admin");
      apiLogger.info("[%s] Admin enrolled successfully", new Date());
    } catch (e) {
      throw e;
    }
  }

  const provider = wallet.getProviderRegistry().getProvider(adminIdentity.type);
  const adminUser = await provider.getUserContext(adminIdentity, "admin");

  let secret;
  try {
    secret = await ca.register(
      { enrollmentID: username, role: "client" },
      adminUser
    );
  } catch (e) {
    throw e;
  }

  const enrollment = await ca.enroll({
    enrollmentID: username,
    enrollmentSecret: secret,
  });

  const x509Identity = {
    credentials: {
      certificate: enrollment.certificate,
      privateKey: enrollment.key.toBytes(),
    },
    mspId: `${organization}MSP`,
    type: "X.509",
  };

  try {
    await wallet.put(username, x509Identity);
    apiLogger.info(
      "[%s] %s enrolled successfully for %s",
      new Date(),
      username,
      organization
    );
    return {
      data: `${username} enrolled successfully for ${organization}`,
    };
  } catch (e) {
    throw e;
  }
};

const enrollAdmin = async (org, ccp) => {
  try {
    const caInfo =
      ccp.certificateAuthorities[`ca.${org.toLowerCase()}.minpass.com`];
    const caTlsCaCerts = caInfo.tlsCACerts.pem;
    const ca = new FabricCAClient(
      caInfo.url,
      { trustedRoots: caTlsCaCerts, verify: false },
      caInfo.caName
    );

    const walletPath = path.join(process.cwd(), `wallet-${org.toLowerCase()}`);
    const wallet = await Wallets.newFileSystemWallet(walletPath);

    const identity = await wallet.get("admin");
    if (identity) {
      apiLogger.info("[%s] Admin identity already exists", new Date());
      return;
    }

    const enrollment = await ca.enroll({
      enrollmentID: "admin",
      enrollmentSecret: "adminpw",
    });

    const x509Identity = {
      credentials: {
        certificate: enrollment.certificate,
        privateKey: enrollment.key.toBytes(),
      },
      mspId: `${org}MSP`,
      type: "X.509",
    };

    await wallet.put("admin", x509Identity);
    apiLogger.info("[%s] Admin enrolled successfully", new Date());
    return;
  } catch (e) {
    throw e;
  }
};

const invokeTransaction = async (
  channel,
  chaincode,
  fn,
  args,
  username,
  organization
) => {
  try {
    const ccp = getCCP(organization);

    const walletPath = path.join(
      process.cwd(),
      `wallet-${organization.toLowerCase()}`
    );
    const wallet = await Wallets.newFileSystemWallet(walletPath);

    const identity = await wallet.get(username);
    if (!identity) {
      throw new Error(`Identity ${username} not found`);
    }

    const connectionOptions = {
      wallet,
      identity: username,
      discovery: {
        enabled: true,
        asLocalhost: true,
      },
      eventHandlerOptions: {
        commitTimeout: 100,
        strategy: DefaultEventHandlerStrategies.NETWORK_SCOPE_ALLFORTX,
      },
    };

    const gateway = new Gateway();
    await gateway.connect(ccp, connectionOptions);

    const network = await gateway.getNetwork(channel);

    const contract = network.getContract(chaincode);

    let result;

    switch (fn) {
      case "createPassword": {
        result = await contract.submitTransaction(
          fn,
          args[0],
          args[1],
          args[2],
          username
        );
        break;
      }
      case "changePassword": {
        result = await contract.submitTransaction(
          fn,
          args[0],
          args[1],
          args[2],
          username
        );
        break;
      }
      case "queryPassword": {
        result = await contract.submitTransaction(
          fn,
          args[0],
          args[1],
          username
        );
        break;
      }
      case "allPasswords": {
        result = await contract.submitTransaction(fn, username);
        break;
      }
      case "addNote": {
        result = await contract.submitTransaction(
          fn,
          username,
          args[2], // body
          args[0], // id
          args[3], // lastModified
          args[1] // title
        );
        break;
      }
      case "updateNote": {
        result = await contract.submitTransaction(
          fn,
          username,
          args[2], // body
          args[0], // id
          args[3], // lastModified
          args[1] // title
        );
        break;
      }
      case "queryNote": {
        result = await contract.submitTransaction(
          fn,
          username,
          args[0] // id
        );
        break;
      }
      case "allNotes": {
        result = await contract.submitTransaction(fn, username);
        break;
      }
      case "uploadFile": {
        result = await contract.submitTransaction(
          fn,
          username,
          args[0], // filename
          args[1] // contents
        );
        break;
      }
      case "allFiles": {
        result = await contract.submitTransaction(fn, username);
        break;
      }
    }

    await gateway.disconnect();

    logger.debug("[%s] ---RESULT---\n%s\n", new Date(), result);

    try {
      return JSON.parse(result.toString());
    } catch {
      logger.debug(
        "[%s] ---ERROR---\n%s\n",
        new Date(),
        "Failed to parse JSON"
      );
      return {
        error: "Invalid response",
      };
    }
  } catch (e) {
    logger.debug("[%s] ---ERROR---\n%s\n", new Date(), e);
    return { error: e.message };
  }
};

module.exports = {
  getUser,
  invokeTransaction,
};
