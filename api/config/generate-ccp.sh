#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    local PP1=$(one_line_pem $6)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s#\${PEERPEM1}#$PP1#" \
        -e "s#\${P0PORT1}#$7#" \
        -e "s/\${org}/${1,,}/" \
        ./ccp-template.json
}

rm ./connection-abc.json
rm ./connection-xyz.json

ORG=Abc
P0PORT=7051
CAPORT=7054
P0PORT1=8051
PEERPEM=../../artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/peers/peer0.abc.minpass.com/msp/tlscacerts/tlsca.abc.minpass.com-cert.pem
PEERPEM1=../../artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/peers/peer1.abc.minpass.com/msp/tlscacerts/tlsca.abc.minpass.com-cert.pem
CAPEM=../../artifacts/channel/crypto-config/peerOrganizations/abc.minpass.com/msp/tlscacerts/tlsca.abc.minpass.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERPEM1 $P0PORT1)" > connection-abc.json
echo -e "\e[1;42m Generated CCP for ${ORG} \e[0m"


ORG=Xyz
P0PORT=9051
CAPORT=8054
P0PORT1=10051
PEERPEM=../../artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/peers/peer0.xyz.minpass.com/msp/tlscacerts/tlsca.xyz.minpass.com-cert.pem
PEERPEM1=../../artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/peers/peer1.xyz.minpass.com/msp/tlscacerts/tlsca.xyz.minpass.com-cert.pem
CAPEM=../../artifacts/channel/crypto-config/peerOrganizations/xyz.minpass.com/msp/tlscacerts/tlsca.xyz.minpass.com-cert.pem

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $PEERPEM1 $P0PORT1)" > connection-xyz.json
echo -e "\e[1;42m Generated CCP for ${ORG} \e[0m"
