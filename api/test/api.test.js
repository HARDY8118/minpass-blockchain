const describe = require("mocha").describe;

const chai = require("chai");
const { expect } = chai;

const chaiHttp = require("chai-http");

chai.use(chaiHttp);

const BASE_URL = "http://127.0.0.1:3000";

describe("Server is reachable", () => {
  it("Ping server", (done) => {
    chai
      .request(BASE_URL)
      .get("/ping")
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.deep.equal({ ping: "pong" });
      });

    chai
      .request(BASE_URL)
      .put("/ping")
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.deep.equal({ ping: "pong" });
        done();
      });
  });
});

describe("Register", () => {
  it("Register new user", (done) => {
    chai
      .request(BASE_URL)
      .post("/register")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("data");
        expect(res.body.data).to.be.oneOf([
          "alice enrolled successfully for Abc",
          "alice already exists in Abc",
        ]);
        done();
      });
  });
  it("Register existing user", (done) => {
    chai
      .request(BASE_URL)
      .post("/register")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("data");
        expect(res.body.data).to.be.equal("alice already exists in Abc");
        done();
      });
  });
});

describe("Invoke Password methods", () => {
  it("Invoking without authorization", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "",
        args: [],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }

        expect(res).to.have.status(401);
        expect(res).to.have.header("www-authenticate");
        expect(res).to.have.header(
          "www-authenticate",
          'Basic realm="Invoke chaincode"'
        );
        done();
      });
  });

  it("Invoking with wrong authorization", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGFzcw==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "",
        args: [],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }

        expect(res).to.have.status(401);
        expect(res).not.have.header("WWW-Authenticate");
        expect(res).not.have.header("www-authenticate");
        done();
      });
  });

  it("Invoking 'createPassword'", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "createPassword",
        args: ["alice", "example.com", "secretpassword"],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("data");
        expect(res.body.data).to.be.equal("Added new password");
        done();
      });
  });
  it("Invoking 'changePassword'", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "changePassword",
        args: ["alice", "example.com", "supersecretpassword"],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("data");
        expect(res.body.data).to.be.equal("Password updated");
        done();
      });
  });
  it("Invoking 'changePassword' for unknown service", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "changePassword",
        args: ["alice", "test.com", "supersecret"],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(404);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("error");
        expect(res.body.error).to.equal("Username or service does not exists");
        done();
      });
  });
  it("Invoking 'changePassword' for unknown username", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "changePassword",
        args: ["bob", "example.com", "supersecret"],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(404);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("error");
        expect(res.body.error).to.equal("Username or service does not exists");
        done();
      });
  });
});

describe("Invoke Note methods", () => {
  it("Invoking 'addNote'", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "addNote",
        args: [
          "d62760-e8fa-7c3e-bcaa-364aad414e7d",
          "Note title",
          "Note Body",
          Date.now(),
        ],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("data");
        expect(res.body.data).to.be.equal("Added new note");
        done();
      });
  });
  it("Invoking 'updateNote'", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "updateNote",
        args: [
          "d62760-e8fa-7c3e-bcaa-364aad414e7d",
          "New Note title",
          "New Note Body",
          Date.now(),
        ],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("data");
        expect(res.body.data).to.be.equal("Note updated");
        done();
      });
  });
  it("Invoking 'updateNote' for unknown id", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "updateNote",
        args: [
          "5a45427-72-80-eb6c-f1caa8245",
          "Updated title",
          "Updated Body",
          Date.now(),
        ],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(404);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("error");
        expect(res.body.error).to.be.equal("Note id not found");
        done();
      });
  });
});

describe("Invoke File methods", () => {
  it("Invoking 'uploadFile'", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "uploadFile",
        args: ["newfile.txt", "File content"],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("data");
        expect(res.body.data).to.be.equal("File uploaded");
        done();
      });
  });
  it("Invoking 'allFiles'", function (done) {
    this.timeout(4000);
    chai
      .request(BASE_URL)
      .post("/invoke")
      .set("content-type", "application/json")
      .set("Authorization", "Basic dGVzdDp0ZXN0UGEkJA==")
      .send({
        channel: "sharedchannel",
        chaincode: "Password",
        fn: "allFiles",
        args: [],
        username: "alice",
        organization: "Abc",
      })
      .end((err, res) => {
        if (err) {
          throw err;
        }
        expect(res).to.have.status(200);
        expect(res).to.have.header("Content-Type", /application\/json/);
        expect(res.body).to.be.a("object");
        expect(res.body).to.have.key("data");
        expect(res.body.data).to.be.a("array");
        expect(res.body.data.length).to.be.greaterThan(0);
        // expect(res.body.data).to.be.equal("Added new note");
        done();
      });
  });
});
