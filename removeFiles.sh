#!/bin/bash

rm -r ./channel-artifacts
# rm -r ./artifacts/chaincode/node_modules
rm -r ./artifacts/channel/crypto-config
rm ./artifacts/channel/genesis.block
rm ./artifacts/channel/sharedchannel.tx
rm ./artifacts/channel/AbcMSPanchors.tx
rm ./artifacts/channel/XyzMSPanchors.tx

# rm -r ./api/node_modules
rm ./api/config/connection-abc.json
rm ./api/config/connection-xyz.json
rm -r ./api/wallet-abc
rm -r ./api/wallet-xyz
# rm -r ./api/logs

rm log.txt
rm CC_VERSION

rm Password.tar.gz

cd artifacts
docker-compose down -v
