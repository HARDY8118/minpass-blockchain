# Installing

```bash
curl -sSL https://bit.ly/2ysbOFE | bash -s -- 2.0.1 1.4.6
```

# Starting

```bash
cd artifacts/channel
./create-artifacts.sh

cd ..
docker-compose up -d

cd ..
./createChannel.sh

nvm use 10.15.2

cd api
npm install

cd ..
./deployChaincode.sh

cd api/config
./generate-ccp.sh

cd ..
node app.js
```

# API endpoints

## /register

Register new user under organization

### Data

| DATA         | TYPE   | DESC                 | DEFAULT |
| ------------ | ------ | -------------------- | ------- |
| username     | string | name of user         |
| organization | string | name of organization |

### Usage

POST /register

```json
{
  "username": "alice",
  "organization": "Abc"
}
```

```json
{
  "username": "bob",
  "organization": "Xyz"
}
```

### Response

**200**

```json
{
  "data": "alice enrolled successfully for Abc"
}
```

```json
{
  "data": "bobo enrolled successfully for Xyz"
}
```

## /invoke

Invoke chaincode


### Authentication
Invoking chaincode requires Basic authentication, 
Refer [Basic Authentication](https://gitlab.com/-/snippets/2328298)

If Authorization header is not provided response `401 Unauthorized is returned` with `www-authenticate: Basic realm="Invoke chaincode"` header. In case wrong authentication details `401 Unauthorized` is returned without `www-authenticate` header.

### Data

| DATA         | TYPE     | DESC                              | DEFAULT       |
| ------------ | -------- | --------------------------------- | ------------- |
| channel      | string   | channel name                      | sharedchannel |
| chaincode    | string   | name of chaincode                 | Passsword     |
| fn           | string   | [function](#functions) name       |
| args         | [string] | array of arguments                |
| username     | string   | username of invoking user         |
| organization | string   | name of organization (Abc or Xyz) | Abc           |


### Functions

### **createPassword**

Add password entry

**Args**

| NAME     | TYPE   | DESCRIPTION                          |
| -------- | ------ | ------------------------------------ |
| username | string | username to store for password       |
| service  | string | service for which password is stored |
| password | string | encrypted password to store          |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "createPassword",
  "args": ["alice123", "example.com", "secretpassword"],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": "Added new password"
}
```

---

### **changePassword**

Change password entry

**Args**

| NAME        | TYPE   | DESCRIPTION                    |
| ----------- | ------ | ------------------------------ |
| username    | string | username for updating password |
| service     | string | service for updating password  |
| newPassword | string | new encrypted password         |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "changePassword",
  "args": ["alice123", "example.com", "$ecretpa5sw0rd"],
  "username": "alice",
  "organization": "Abc"
}
```

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "changePassword",
  "args": ["alice123", "example2.com", "$ecretpa5sw0rd"],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": "Password updated"
}
```

**404**

If no combination found

```json
{
  "error": "Username or service does not exists"
}
```

---

### **queryPassword**

Fetch all versions of password

**Args**

| NAME     | TYPE   | DESCRIPTION                    |
| -------- | ------ | ------------------------------ |
| username | string | username for updating password |
| service  | string | service for updating password  |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "queryPassword",
  "args": ["alice123", "example.com"],
  "username": "alice",
  "organization": "Abc"
}
```

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "changePassword",
  "args": ["alice123", "example2.com", "$ecretpa5sw0rd"],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": [
    {
      "owner": "alice",
      "password": "$ecretpa5sw0rd",
      "service": "example.com",
      "type": "password",
      "username": "alice123"
    },
    {
      "username": "alice123",
      "service": "example.com",
      "password": "secretpassword",
      "type": "password",
      "owner": "alice"
    }
  ]
}
```

**200**

If no combination found

```json
{
  "data": []
}
```

---

### **allPasswords**

Fetch all current passwords for user

**Args**

| NAME | TYPE | DESCRIPTION |
| ---- | ---- | ----------- |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "allPasswords",
  "args": [],
  "username": "alice",
  "organization": "Abc"
}
```

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "allPasswords",
  "args": [],
  "username": "bob",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": [
    {
      "owner": "alice",
      "password": "anotherencryption",
      "service": "example.com",
      "type": "password",
      "username": "alice"
    },
    {
      "owner": "alice",
      "password": "encryptedpassword",
      "service": "example1.com",
      "type": "password",
      "username": "alice"
    },
    {
      "owner": "alice",
      "password": "encryptedpassword",
      "service": "example2.com",
      "type": "password",
      "username": "alice"
    }
  ]
}
```

**404**

If invalid username

```json
{
  "error": "Identity bob not found"
}
```

---

### **addNote**

Add a new note

**Args**

| NAME         | TYPE   | DESCRIPTION     |
| ------------ | ------ | --------------- |
| id           | string | unique note id  |
| title        | string | title of note   |
| body         | string | note body       |
| lastModified | number | timestamp in ms |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "addNote",
  "args": [
    "d62760-e8fa-7c3e-bcaa-364aad414e7d",
    "Note title",
    "Note body",
    1652033471866
  ],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": "Added new note"
}
```

---

### **updateNote**

Update note data

**Args**

| NAME         | TYPE   | DESCRIPTION                           |
| ------------ | ------ | ------------------------------------- |
| id           | string | unique note id                        |
| title        | string | updated title of note                 |
| body         | string | updated note body                     |
| lastModified | number | timestamp in ms for last modification |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "updateNote",
  "args": [
    "d62760-e8fa-7c3e-bcaa-364aad414e7d",
    "New Note title",
    "New Note body",
    1652034169136
  ],
  "username": "alice",
  "organization": "Abc"
}
```

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "updateNote",
  "args": [
    "5a45427-72-80-eb6c-f1caa8245",
    "New Note title",
    "New Note body",
    1652034169136
  ],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": "Added new note"
}
```

**404**

```json
{
  "error": "Note id not found"
}
```

---

### **queryNote**

Query note versions

**Args**

| NAME | TYPE   | DESCRIPTION    |
| ---- | ------ | -------------- |
| id   | string | unique note id |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "queryNote",
  "args": ["d62760-e8fa-7c3e-bcaa-364aad414e7d"],
  "username": "alice",
  "organization": "Abc"
}
```

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "queryNote",
  "args": ["5a45427-72-80-eb6c-f1caa8245"],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": [
    {
      "body": "New Note Body",
      "id": "d62760-e8fa-7c3e-bcaa-364aad414e7d",
      "lastModified": "1652034169136",
      "owner": "alice",
      "title": "New Note title",
      "type": "note"
    },
    {
      "body": "Note Body",
      "id": "d62760-e8fa-7c3e-bcaa-364aad414e7d",
      "lastModified": "1652033471866",
      "title": "Note title",
      "type": "note",
      "owner": "alice"
    }
  ]
}
```

**200**
If invalid note id

```json
{
  "data": []
}
```

---

### **allNotes**

Query note versions

**Args**

| NAME | TYPE | DESCRIPTION |
| ---- | ---- | ----------- |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "allNote",
  "args": [],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": [
    {
      "body": "New Note Body",
      "id": "d62760-e8fa-7c3e-bcaa-364aad414e7d",
      "lastModified": "1652034169136",
      "owner": "alice",
      "title": "New Note title",
      "type": "note"
    }
  ]
}
```

**200**
If invalid note id

```json
{
  "data": []
}
```

---

### **uploadFile**

Upload new file

**Args**

| NAME | TYPE | DESCRIPTION |
| ---- | ---- | ----------- |
|filename|string|name of file|
|content|string|file contents|

**Usage**
POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "uploadFile",
  "args": [
    "newfile.txt",
    "File contents",
  ],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": "File uploaded"
}
```
---

### **allFiles**

Get all files by username

**Args**

| NAME | TYPE | DESCRIPTION |
| ---- | ---- | ----------- |

**Usage**

POST /invoke

```json
{
  "channel": "sharedchannel",
  "chaincode": "Password",
  "fn": "allFiles",
  "args": [],
  "username": "alice",
  "organization": "Abc"
}
```

**Response**

**200**

```json
{
  "data": [
    {
      "filename": "newfile.txt",
      "content": "File contents",
      "owner": "alice",
      "type": "file"
    }
  ]
}
```
